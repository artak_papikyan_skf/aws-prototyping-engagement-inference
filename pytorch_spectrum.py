import argparse
import json
import logging
import os
import sys
import time

import numpy as np
import pandas as pd

import torch
from torch import nn
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import Adam


# from imblearn.over_sampling import SMOTE

from sklearn.metrics import confusion_matrix

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

def model_fn(model_dir):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = Autoencoder(792)
    with open(os.path.join(model_dir, "model.pth"), "rb") as f:
        model.load_state_dict(torch.load(f))
    return model.to(device)

def predict_fn(input_data, model):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.to(device)
    model.eval()
    with torch.no_grad():
        return model(input_data.to(device))
    
def preparation_vsa_autoencoder_input(df:pd.DataFrame,
                                      ski_slope_thd:int = 8,
                                      spectrum_colname:str = 'fftdata',
                                      len_spectrum_name:str = 'fftn',
                                      label_colname:str = 'status')->dict:
    #check the len of fftdata, if we have diff length, we have to normalize by min:
#     n_differ_len_of_fftdata = df[len_spectrum_name].unique().size
#     fft_min_len = int(df[len_spectrum_name].min())
#     print(fft_min_len)
#     if n_differ_len_of_fftdata > 1:
#         df_copy = df.copy()
#         df_copy[spectrum_colname] = df[spectrum_colname].apply(lambda row: row[ski_slope_thd:fft_min_len])
#         df = df_copy
        
        
    array = df[[spectrum_colname, label_colname]].values

    X = np.stack(array[:,0])
    y = array[:,1].astype(int)

    return {'X':X, 'y':y}

def train(device,
          args)->object:
    
    df = pd.read_parquet(args.data_dir + '/train.parquet.gzip')
    input_data = preparation_vsa_autoencoder_input(df)
    x_train = input_data['X']
    
    #check the lenght of each sample (sould be the same)
    
    
    y_train = input_data['y']
    sample_size = x_train[0].size
#     sample_size = args.sample_size
#     print(sample_size)
    # 0. Default
    criterion = nn.MSELoss()
    
    # 1. Train data oversampling
#     if args.smote_flag:
#          oversample = SMOTE()
#          x_train, y_train = oversample.fit_resample(args.x_train, args.y_train)
    

    # 1. Train data preparation
    objective_label = 1#0-default
    train_in_distribution = x_train[y_train == objective_label]
    train_in_distribution = torch.tensor(train_in_distribution.astype(np.float32))
    train_in_dataset = TensorDataset(train_in_distribution)
    train_in_loader = DataLoader(train_in_dataset, batch_size=args.batch_size, shuffle=True)
    
    # 2. Model initialization
    model = Autoencoder(sample_size).to(device)
    optimizer = Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    
    # 3. Training of model
    epoch = 0
    
    running_loss = args.running_loss
    while (running_loss > args.loss_thd) and (epoch < args.thd_epoch):
        running_loss = 0
        for (x_batch, ) in train_in_loader:
            x_batch = x_batch.to(device)

            output = model(x_batch)
            loss = criterion(output, x_batch)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            running_loss += loss.item()
        print("epoch [{}/{}], train loss:{:.4f}".format(epoch+1, args.thd_epoch, running_loss))
        epoch += 1
    logger.info("epoch [{}/{}], train loss:{:.4f}".format(epoch+1, args.thd_epoch, running_loss))
    
    return model
    # test(model, test_loader, device)
        
    # save_model(model, args.model_dir)
    
def save_model(model, model_name, args):
    logger.info("Saving the model.")
    path = os.path.join(args.model_dir, model_name)
    # recommended way from http://pytorch.org/docs/master/notes/serialization.html
    torch.save(model.cpu().state_dict(), path)
    

class Autoencoder(nn.Module):
    
    def __init__(self, input_size):
        self.dropout_fraction = 0.5
        super(Autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(input_size, 400), #input = 800, latency space = 50 neurons
            nn.Dropout(self.dropout_fraction),
            nn.LeakyReLU(0.2),
            nn.Linear(400, 200),
            nn.LeakyReLU(0.2),
            nn.Linear(200, 100),
            nn.LeakyReLU(0.2),
            nn.Linear(100, 50),
            nn.LeakyReLU(0.2),)
        self.decoder = nn.Sequential(
            nn.Linear(50, 100),
            nn.LeakyReLU(0.2),
            nn.Linear(100, 200),
            nn.LeakyReLU(0.2),
            nn.Linear(200, 400),
            nn.LeakyReLU(0.2),
            nn.Linear(400, input_size),
            nn.Dropout(self.dropout_fraction),
            nn.LeakyReLU(0.2),)

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x

if __name__ == "__main__":
    
    if torch.cuda.is_available():
      device = torch.device("cuda")
    else:
      device = torch.device("cpu")
    
    parser = argparse.ArgumentParser()
    
    # Data and model checkpoints directories
    parser.add_argument(
        "--batch-size",
        type=int,
        default=300,
        metavar="N",
        help="input batch size for training (default: 300)",
    )
    parser.add_argument(
        "--thd_epoch",
        type=int,
        default=20,
        metavar="N",
        help="thd for number of epochs to train (default: 20)",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=20,
        metavar="N",
        help="number of epochs to train (default: 10)",
    )
    parser.add_argument(
        "--lr", type=float, default=0.001, metavar="LR", help="learning rate (default: 0.001)"
    )
    parser.add_argument(
        "--loss_thd", type=float, default=0.5, metavar="loss_thd", help="loss_thd for early stop (default: 0.1)"
    )
    parser.add_argument("--running_loss", type=int, default=30, metavar="rl", help="start loss for  (default: 1)")
    parser.add_argument(
        "--objective_label",
        type=int,
        default=1,
        metavar="N",
        help="objective label for model training",
    )
    parser.add_argument(
        "--smote_flag",
        type=str,
        default=True,
        help="synthetic oversampling flag",
    )
    parser.add_argument(
        "--weight_decay", type=float, default = 1e-5, metavar="WD", help="weight_decay (default: 1e-5)")
    
    parser.add_argument(
        "--backend",
        type=str,
        default=None,
        help="backend for distributed training (tcp, gloo on cpu and gloo, nccl on gpu)",
    )

    # Container environment
    parser.add_argument("--hosts", type=list, default=json.loads(os.environ["SM_HOSTS"]))
    parser.add_argument("--current-host", type=str, default=os.environ["SM_CURRENT_HOST"])
    parser.add_argument("--model-dir", type=str, default=os.environ["SM_MODEL_DIR"])
    parser.add_argument("--data-dir", type=str, default=os.environ["SM_CHANNEL_TRAINING"])
    parser.add_argument("--num-gpus", type=int, default=os.environ["SM_NUM_GPUS"])
    args = parser.parse_args()
    model = train(device, args)

#     group_idx = 23
#     model_dir = './'
    
    #3 Save of model
    with open(os.path.join(args.model_dir, 'model.pth'), 'wb') as f:
        torch.save(model.state_dict(), f)
        
#     save_model(model, model_name, args)